import {getHeader} from '../util/netutil';

const URL = require('url').URL;

const AUDIOANDVIDEO = 'audioandvideo';
const AUDIOONLY = 'audioonly';
const VIDEONLY = 'videoonly';

export { AUDIOANDVIDEO, AUDIOONLY, VIDEONLY };

export default class Download {

	get title() { return this._title; }
	set title(v) { this._title = v;}

	/**
	 * @property {number} size
	 */
	get size() {
		return this._size;
	}
	set size(v) { this._size = v; }

	/**
	 * @property {URL} url - url of download.
	 */
	get url() { return this._url; }
	set url(v) {
		if ( v instanceof URL ) this._url = v;
		else {
			try {
				v = new URL(v);
				this._url = v;
			}catch(e){}
		}
	}

	/**
	 * @property {number} received - total bytes received.
	 */
	get received() { return this._received; }
	set received(v) { this._received = v; }

	/**
	 * @property {stream.Readable} reader - Stream which reads from source.
	 */
	get reader() { return this._reader; }
	set reader(v) { this._reader = v;}

	/**
	 * @property {stream.Writable} writer - Stream that writes to dest.
	 */
	get writer() { return this._writer; }
	set writer(v) { this._writer =v;}

	/**
	 * @property {string} path - path to save file.
	 */
	get path() { return this._path; }
	set path(v) { this._path = v;}

	/**
	 * @property {number} progress - download progress percent out of 100.
	 */
	get progress() {
		return this._size ? Math.floor( 100* this._received/this._size ) : 0;
	}

	/**
	 * @property {string} saveFormat - final save format for the file.
	 */
	get saveFormat() { return this._saveFormat; }
	set saveFormat(v) { this._saveFormat = v; }

	/**
	 * @property {ytdl.videoFormat } form - format to download.
	 */
	get format() { return this._format; }
	set format(v) { this._format = v;}

	/**
	 * @property {ytdl.videoInfo} info - video info for
	 * downloadFromInfo()
	 */
	get info() { return this._info; }
	set info(v) { this._info = v;}

	/**
	 * @property {string} channels - channels to include in final save.
	 */
	get channels() { return this._channels; }
	set channels(v) { this._channels = v;}

	/**
	 * @property {string|'create'|'running'|'error'|'done'|'cancelled'} state - state of the download.
	 * 'create' indicates the download object is being constructed/edited before download begins.
	 */
	get state() { return this._state;}
	set state(v) { this._state = v;}

	get isComplete() { return this._state === 'done' }
	get isCancelled() {return this._state === 'cancelled'}
	get isRunning() { return this._state === 'running'}

	/**
	 * @property {boolean} isYoutube - true if the url source is youtube.
	 */
	get isYoutube() { return this._youtube; }

	/**
	 * 
	 * @param {?Object} [opts=null] 
	 */
	constructor( opts=null ) {

		this._url = null;
		this._reader = this._writer = null;

		this._channels = AUDIOANDVIDEO;
		this._title = '';

		this._info = null;
		this._format = null;
		this._saveFormat = '';

		this._size = 0;
		this._path = '';

		this._youtube = true;

		this._state = 'create';
		this._received = 0;

		if ( opts ) Object.assign( this, opts );

	}

	cancel() {

		if ( this._reader != null ) {
			this._reader.destroy( new Error('Download Cancelled'));
		}
		this._state = 'cancelled';
	}

	parseHeaders( headers ) {

		if ( headers ){
			let size = Number( getHeader( 'content-length', headers) );
			this._size = !Number.isNaN(size) ? size : 0;
		}

	}

	/**
	 * Set the initial Server Response
	 * @param {*} resp 
	 */
	setResponse( resp ) {

		if ( resp.headers ) {

			let size = Number( getHeader( 'content-length', resp.headers));
			this._size = !Number.isNaN(size) ? size : 0;

		}

	}


}