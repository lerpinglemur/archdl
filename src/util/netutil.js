
/**
 * Get the value of a named header.
 * Some server responses do not appear to supply this function.
 * @param {string} name - name of header to get.
 * @param {*} headers - array of all headers.
 * @returns {*} header value, or null.
 */
export function getHeader( name, headers ){

	name = name.toLowerCase();
	for( let p in headers ) {
		if ( p.toLowerCase() === name ) return headers[p];
	}
	return null;

}