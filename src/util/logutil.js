export function logAllProps(obj, proto=true) {

	if ( !proto ) console.log( Object.getOwnPropertyNames(obj) );
	else console.log( allProps(obj));

}

export function allProps(obj) {

	let proto = Object.getPrototypeOf(obj);
	if ( proto && proto != obj ) return Object.getOwnPropertyNames(obj).concat( allProps(proto) );
	return Object.getOwnPropertyNames(obj);

}

export function logObj( obj ){

	let usedMap = new Map();
	console.log( getString(obj, usedMap) );

}

function getString( obj, usedMap ) {
	
	let s = '';

	let t,v;

	usedMap.set(obj, true)

	for( let p in obj ) {

		v = obj[p];
		t = typeof v;
		if ( !v || t === 'string' || t === 'number' || t === 'boolean') {
			s += p + ': ' + v + '\n';
		} else if ( t === 'object' ) {
			if ( usedMap.has(v) ) {
				s += p + ': [back ref]\n';
				continue;
			}
			s += p + ':\n' + getString(v, usedMap );
		} else s += p + ': unknown';

	}

	return s;

}