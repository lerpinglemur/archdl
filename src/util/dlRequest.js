export default class DownloadRequest {

	get title() { return this._title; }
	set title(v) { this._title = v;}

	/**
	 * Expected size of download.
	 * @property {number} size
	 */
	get size() { return this._size; }
	set size(v) { this._size = v; }

	/**
	 * @property {URL} url - url of download.
	 */
	get url() { return this._url; }
	set url(v) {
		if ( v instanceof URL ) this._url = v;
		else {
			try {
				v = new URL(v);
				this._url = v;
			}catch(e){}
		}
	}

	/**
	 * @property {string} path - path to save file.
	 */
	get path() { return this._path; }

	get destFormat() { return this._format; }

	/**
	 * @property {string} channels - channels to include in final save.
	 */
	get channels() { return this._channels; }

	/**
	 * @property {boolean} isYoutube - true if the url source is youtube.
	 */
	get isYoutube() { return this._youtube; }

	constructor() {

		this._url = url;
		this._source = this._dest = null;
		this._channels = AUDIOANDVIDEO;
		this._title = '';
		this._format = '';
		this._size = 0;
		this._path = '';

		this._youtube = false;


	}

}