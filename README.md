Simple UI Wrapper for ytdl using electron and Vue.

Electron makes the resulting app disappointingly large for such a simple application,
but can easily be extended to a more full-featured application which might justify
the space.

Note that you will need a native implementation of ffmpeg for the program to run.